$(document).ready(function () {

    var elev1=0;
    var elev2=0;
    var listElev1=[];
    var listElev2=[];
    
    function closeElevator(elevator){
        setTimeout(function(){
            elevator.attr('src', 'static/images/opening_elevator.png');
        },50);
        setTimeout(function(){
            elevator.attr('src', "static/images/closing_elevator.png");
        },100);
        setTimeout(function(){
            elevator.attr('src', "static/images/closed_elevator.png");
        },150);
    }
    
    function openElevator(elevator){
        setTimeout(function(){
            elevator.attr('src', 'static/images/closing_elevator.png');
        },50);
        setTimeout(function(){
            elevator.attr('src', 'static/images/opening_elevator.png');
        },100);
        setTimeout(function(){
            elevator.attr('src', 'static/images/open_elevator.png');
        },150);
    }
    
    function goUp(elevator){
        let elevatorName=elevator.attr('id');
        $.get(`http://127.0.0.1:5000/${elevatorName}/go_up`, function(data){});
        closeElevator(elevator);
        setTimeout(function(){
            elevator.animate({
                'margin-top':'-=200px',
            },{
                duration: 2000,
                complete: function (){
                    openElevator(elevator);
                }
            })
        },200);
    }
    
    function goDown(elevator){
        let elevatorName=elevator.attr('id');
        $.get(`http://127.0.0.1:5000/${elevatorName}/go_down`, function(data){});
        closeElevator(elevator);
        setTimeout(function(){
            elevator.animate({
                'margin-top':'+=200px',
            },{
                duration: 2000,
                complete: function (){
                    openElevator(elevator);
                }
            });
        },200);
    }
    
    function goFrom0To2(elevator){
        let elevatorName=elevator.attr('id');
        $.get(`http://127.0.0.1:5000/${elevatorName}/go_up`, function(data){});
        $.get(`http://127.0.0.1:5000/${elevatorName}/go_up`, function(data){});
        closeElevator(elevator);
        setTimeout(function(){
            elevator.animate({
                'margin-top':'-=400px',
            },{
                duration: 4000,
                complete: function (){
                    openElevator(elevator);
                }
            })
        },200);
    }
    
    function goFrom2To0(elevator){
        let elevatorName=elevator.attr('id');
        $.get(`http://127.0.0.1:5000/${elevatorName}/go_down`, function(data){});
        $.get(`http://127.0.0.1:5000/${elevatorName}/go_down`, function(data){});
        closeElevator(elevator);
        setTimeout(function(){
            elevator.animate({
                'margin-top':'+=400px',
            },{
                duration: 4000,
                complete: function (){
                    openElevator(elevator);
                }
            })
        },200);
    }
    
    function processListElev1(){
        let floor = listElev1.shift();
        let elevator=$("#elev-1");
        if (floor == 2){
            goToFloor2(elevator, elev1);
            elev1=2;
        }
        else if (floor == 1){
            goToFloor1(elevator,elev1);
            elev1=1;
        }
        else if (floor == 0){
            goToFloor0(elevator, elev1);
            elev1=0;
        }
    }

    function processListElev2(){
        let floor = listElev2.shift();
        let elevator=$("#elev-2");
        if (floor == 2){
            goToFloor2(elevator, elev2);
            elev2=2;
        }
        else if (floor == 1){
            goToFloor1(elevator,elev2);
            elev2=1;
        }
        else if (floor == 0){
            goToFloor0(elevator, elev2);
            elev2=0;
        }
    }

    function goToFloor2(elevator, elevVar){
        if (elevVar==1){
            goUp(elevator);
        }   
        else if (elevVar==0){
            goFrom0To2(elevator);
        }
    }

    function goToFloor1(elevator, elevVar){
        if (elevVar==2){
            goDown(elevator);
        }
        else if (elevVar==0){
            goUp(elevator);
        }
    }

    function goToFloor0(elevator, elevVar){
        if (elevVar==2){
            goFrom2To0(elevator);
        }
        else if (elevVar==1){
            goDown(elevator);    
        }
    }
    
    $("#elev1-2").click(function(){
        listElev1.push(2);
        processListElev1();
    });
    
    $("#elev1-1").click(function (){
        listElev1.push(1);
        processListElev1();
    });
    
    $("#elev1-0").click(function (){
        listElev1.push(0);
        processListElev1();
    });

    $("#elev2-2").click(function (){
        listElev2.push(2);
        processListElev2();
    });
    
    $("#elev2-1").click(function (){
        listElev2.push(1);
        processListElev2();
    });
    
    $("#elev2-0").click(function (){
        listElev2.push(0);
        processListElev2();
    });
    
});