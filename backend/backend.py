from flask import Flask, render_template, jsonify
app = Flask(__name__)

@app.route('/<elevatorName>/go_up')
def goUp(elevatorName):
    '''Backend function to move the elevator up one floor. Return a JSON {"up":[elevatorName]}'''
    # add the code needed to operate the elevator here. Operate it on <elevatorName>
    response = jsonify(up=elevatorName)
    return response

@app.route('/<elevatorName>/go_down')
def goDown(elevatorName):
    '''Backend function to move the elevator down one floor. Return a JSON {"down":[elevatorName]}'''
    # add the code needed to operate the elevator here. Operate it on <elevatorName>
    response = jsonify(down=elevatorName)
    return response

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=(5000))