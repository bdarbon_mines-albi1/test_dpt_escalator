# Read me - Test Digital Process Tools

## Sujet du test

Votre client vous propose de développer un software pour faire fonctionner des ascenseurs. Les différents hardwares qui équiperont ces ascenseurs possèdent 2 commandes : goUp() et goDown().

Écrire dans le langage serveur de votre choix le software permettant de faire fonctionner les ascenseurs équipés de ces hardwares.

Votre client vous donne jusqu'à lundi prochain 14H pour résoudre cette mission.

## Architecture de l'archive

L'archive est constituée de 2 dossiers :
- Le dossier backend contient un fichier python `backend.py`. Ce fichier sert à mettre en route un serveur Flask (framework Python) qui possède les 2 commandes goUp() et goDown() dont le frontend fait appel.
- Le dossier frontend contient le fichier html des ascenseurs `elevator.html` ainsi que tous les fichiers static nécessaires pour le bon affichage de l'interface (les images, le fichier CSS et le fichier JS).

## Lancement du backend

Pour pouvoir lancer le serveur Flask, il faut au préalable installer Flask dans un environnement virtuel dédié au test.

On commence par créer l'environnement virtuel que l'on active ensuite:

    % python -m venv venv-elevator
    % source venv-elevator/bin/activate

Ensuite on installe Flask dans ce même environnement:

    % pip install Flask

Il faut ensuite préciser à Flask qu'il faut se placer en mode développement :

    % export FLASK_ENV=development

Il ne reste qu'à exécuter le fichier python pour lancer le serveur :

    % python backend/backend.py

## Lancement du frontend

Il suffit d'ouvrir le fichier `escalator.html` du dossier frontend dans un navigateur web.

## Fonctionnement

Lorsque l'on appuie sur un bouton d'un ascenseur, l'ascenseur correspondant se déplace à l'étage indiqué par le bouton. Il est possible d'appuyer sur plusieurs boutons à la suite, et l'ascenseur va aller aux différents étages dans l'ordre indiqué.

A chaque fois qu'on demande à un ascenseur de se déplacer d'un étage, on envoi une requête GET au serveur de la forme `/<ascenseur>/<order>` qui permet au hardware de faire se déplacer l'ascenseur. Se déplacer de 2 étages d'un coup (par exemple de l'étage 0 à l'étage 2) envoi 2 requêtes au serveur.

## Mea culpa

Vous remarquerez sûrement un bug d'ouverture / fermeture des ascenseurs lorsque vous appuyez sur un bouton alors que l'ascenseur est encore en marche. Je sais d'où vient le problème mais je n'ai pas réussi à le corriger dans le temps imparti. Veuillez m'en excuser.